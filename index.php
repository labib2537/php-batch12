<?php
$title = "The Cow";
$Description = "The cow is a domestic animal. It has four legs, two eyes, two horns, two ears and one tail.";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>php Practice (batch-12)</title>
</head>

<body>
    <h1><?php echo $title; ?></h1>
    <p><?php echo $Description; ?></p>

    <?php

    var_dump($Description);
    echo "<hr>";

    $a = 5;
    $b = 4;
    $c = $a + $b;
    echo $a, " + ", $b, " = ";
    echo $c;
    echo "<hr>";

    function fact($n)
    {
        if ($n < 0) return 0;
        if ($n == 0) return 1;
        else return $n * fact($n - 1);
    }
    echo "Factorial of 5: ";
    echo fact(5);
    echo "<br>";
    echo "Factorial of 6: ";
    echo fact(6);
    echo "<br>";
    echo "Factorial of 7: ";
    echo fact(7);
    echo "<br>";
    echo "Factorial of 8: ";
    echo fact(8);
    echo "<br>";
    echo "Factorial of 9: ";
    echo fact(9);
    echo "<br>";

    echo "<hr>";





    function calculation($a, $b)
    {

        echo $a, " + ", $b, " = ", $a + $b, "<br>";
        echo $a, " - ", $b, " = ", $a - $b, "<br>";
        echo $a, " * ", $b, " = ", $a * $b, "<br>";
        echo $a, " / ", $b, " = ", $a / $b, "<br>";
    }

    calculation(50, 10);
    echo "<br>";
    calculation(100, 40);
    echo "<br>";
    calculation(90, 30);
    echo "<br>";
    calculation(150, 60);
    echo "<br>";

    echo "<hr>";


    function counting($n)
    {
        for ($i = 1; $i <= 10; $i++) {
            echo $n, " X ", $i, " = ", $n * $i, "<br>";
        }
    }
    counting(7);
    echo "<br>";
    counting(13);
    echo "<br>";
    counting(27);
    echo "<br>";
    counting(17);
    echo "<br>";
    counting(43);
    echo "<br>";
    echo "<hr>";



    $var1=75; $var2=25;

    echo $var1+$var2; echo "<br>";
    echo $var1-$var2; echo "<br>";
    echo $var1*$var2; echo "<br>";
    echo $var1/$var2; echo "<br>";
    echo $var1%$var2; echo "<br>";
    echo $var1&$var2; echo "<br>";
    echo $var1.$var2; echo "<br>";

    echo "<hr>";



    function my_function($n1, $n2){
      
          return $n1+$n2;
    }

  

    function my_function2($n1, $n2){
        return $n1-$n2;
  }
  $res1 = my_function(10,5);
  $res2 = my_function2(10,5);

  var_dump($res1 + $res2);
  echo "<hr>";

  function my_function3($n1, $n2, $s){
    
    if($s=='+') echo $n1+$n2;
    if($s=='-') echo $n1-$n2;
    if($s=='*') echo $n1*$n2;
    if($s=='/') echo $n1/$n2;
}

my_function3(10,5,'+'); echo "<br>";
my_function3(10,5,'*'); echo "<br>";
my_function3(10,5,'-'); echo "<br>";
my_function3(10,5,'/'); echo "<br>";


echo "<hr>";

printf("hello!");

echo "<hr>";

define("pi", 3.1416);

function circle_circumference($r){
    echo 2*pi*$r . "<br>";
}

circle_circumference(2);
circle_circumference(5);
circle_circumference(7);


echo "<hr>";

echo "*** String ***" . "<br><br>";

echo "Implode: " . "<br>";

echo implode(' ',["Farhan", "Labib", "Karib"]);

echo "<br>";
echo "<hr>";

echo "Explode: "; echo "<br>";

$input1 = "hello";
$input2 = "hello,farhan,labib";
$input3 = ',';
var_dump( explode( ',', $input1 ) ); echo "<br>";
var_dump( explode( ',', $input2 ) ) ; echo "<br>";
var_dump( explode( ',', $input3 ) ); echo "<br>";
echo "<hr>";

echo "md5: " . "<br>";

$str = 'apple';
$str1 = 'labib';

if (md5($str) === '1f3870be274f6c49b3e31a0c6728957f') {
    echo "Would you like a green or red apple?";
}else{
    echo "Not matched!";
}
echo "<br>";

if (md5($str1) === '1f3870be274f6c49b3e31a0c6728957f') {
    echo "Would you like a green or red apple?";
}else{
    echo "Not matched!";
}


echo "<br>";
echo "<hr>";

echo "nlb2r: "; echo "<br>";
$string = "This\nis\nFarhan\nLabib";
echo $string;
echo "<br>";
echo nl2br($string);

echo "<hr>";


echo "str_pad: "; echo "<br>";
echo str_pad("labib", 10, "p", STR_PAD_BOTH ); echo "<br>";
echo str_pad("labib", 6, "p", STR_PAD_BOTH ); echo "<br>";
echo str_pad("labib", 8, "p", STR_PAD_BOTH ); echo "<br>";

echo "<br>";
echo "<hr>";

echo "str_repeat: "; echo "<br>";

echo str_repeat("farhanlabib",12);
echo "<br>";
echo "<hr>";

echo "str_replace: "; echo "<br>";

$str = "Farhan Labib";
echo $str . "<br>";
echo str_replace("Labib","Karib",$str);
echo "<br>";
echo "<hr>";


$fruits = ['mango', 'apple', 'banana', 'watermelon', 'apple', 'grap'];


echo count($fruits) . "<br>";

foreach($fruits as $fruit){
    echo $fruit . "<br>";
    echo "Length of String: " . strlen($fruit) . "<br>";
}


echo "<hr>";


foreach($fruits as $fruit){
    echo var_dump($fruit). "<br>";
}
echo "<hr>";

echo "strpos: "; echo "<br>";

$mystring = 'farhan labib';
$findme   = 'labib';
$pos = strpos($mystring, $findme);

if ($pos === false) {
    echo "The string '$findme' was not found in the string '$mystring'";
} else {
    echo "The string '$findme' was found in the string '$mystring'";
    echo " and exists at position $pos";
}

echo "<br>";
echo "<hr>";

echo "Lower & Upper: "; echo "<br>";

$str = "farhan labib";
$str1 = "Farhan Karib";

echo strtoupper($str) . "<br>";
echo strtolower($str) . "<br>";
echo "<hr>";

echo "substr: <br>";

$rest = substr("abcdef", -1); echo $rest;   echo "<br>";
$rest = substr("abcdef", -2);  echo $rest; echo "<br>"; 
$rest = substr("abcdef", -3, 1); echo $rest; echo "<br>";
$rest = substr("abcdef", -5, 3); echo $rest; echo "<br>";

echo "<br>";
echo "<hr>";

echo "trim: <br>";

$name = '  farhan labib      ';
echo $name . "<br>";
echo trim($name);

echo "<br>";
echo "<hr>";

echo "ucfirst: <br>";

$name = 'farhan labib';
echo $name . "<br>";

echo ucfirst($name);

echo "<br>";
echo "<hr>";

echo "bangabondu";



    ?>
</body>

</html>
